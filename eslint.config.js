import babelParser from "@babel/eslint-parser";

export default [
  {
    languageOptions: {
      ecmaVersion: "latest",
      sourceType: "module",
      parser: babelParser,
      parserOptions: {
        requireConfigFile: false,
        babelOptions: {
          babelrc: false,
          configFile: false,
          plugins: ["@babel/plugin-syntax-import-assertions"],
        },
      },
    },
    rules: {
      indent: ["error", "tab"],
      quotes: ["error", "double"],
      semi: ["error", "always"],
      curly: "error",
      eqeqeq: "error",
      radix: "error",
      camelcase: [
        "error",
        {
          properties: "always",
          ignoreImports: false,
          ignoreGlobals: false,
        },
      ],
      "linebreak-style": ["error", "unix"],
      "eol-last": ["error", "always"],
      "default-case": "error",
      "default-case-last": "error",
      "no-multiple-empty-lines": "error",
      "no-self-compare": "error",
      "no-alert": "error",
      "no-console": "error",
      "no-use-before-define": "error",
      "no-empty-function": "error",
      "no-magic-numbers": "error",
      "no-nested-ternary": "error",
      "no-var": "error",
      "prefer-arrow-callback": "error",
      "prefer-const": "error",
      "max-len": ["error", 100],
      "array-bracket-spacing": [
        "error",
        "always",
        {
          objectsInArrays: false,
          arraysInArrays: false,
        },
      ],
      "brace-style": [
        "error",
        "1tbs",
        {
          allowSingleLine: true,
        },
      ],
      "func-style": [
        "error",
        "declaration",
        {
          allowArrowFunctions: true,
        },
      ],
    },
  },
];
