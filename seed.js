import mongoose from "mongoose";
import product from "./src/models/product.js";
import dotenv from "dotenv";
dotenv.config();

import allData from "./src/final-data.json" assert { type: "json" };

function getUri() {
  if (process.env.ENV === "PROD") {
    return process.env.PROD_URI;
  } else if (process.env.ENV === "TEST") {
    return process.env.TEST_URI;
  } else {
    return process.env.URI;
  }
}

const uri = getUri();

async function main() {
  await mongoose
    .connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      const productSeed = async () => {
        await product.deleteMany({});
        await product.insertMany(allData);
      };

      productSeed().then(() => {
        console.log("DB filled");
        mongoose.connection.close();
      });
    });
}

main().catch((err) => console.log("!!DBERROR!!", err));
