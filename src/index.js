import express from "express";
import cors from "cors";
import main from "./db.js";
import badRoutes from "./middleware/badRoutes.js";
import productRouter from "./routes/productRoutes.js";
import userRouter from "./routes/userRoutes.js";
import authRouter from "./routes/authRoutes.js";

const app = express();

app.use(cors());
app.use(express.json());
app.use(badRoutes);

app.use(authRouter);
app.use(productRouter);
app.use(userRouter);

app.listen(4000, () => {
  console.log(`Dummy API Launched on port 4000`);
  try {
    main();
  } catch (err) {
    //TODO: Do Error Gestion
    console.error(err);
  }
});
