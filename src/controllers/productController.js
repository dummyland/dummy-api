import log from "loglevel";

import product from "../models/product.js";
import CODES from "../constantes/httpCodes.js";

const ZERO = 0;
const { SUCCESS, CREATED, NOT_FOUND, BAD_REQUEST, INTERNAL_SERVER_ERROR } =
  CODES;

const productController = {
	getAllProduct: async (req, res) => {
		const { limit, page } = req.query;

		const pageNumber = Number.parseInt(page, 10);
		const limitNumber = Number.parseInt(limit, 10);

		let pages = 0;

		if (pageNumber > ZERO && !Number.isNaN(pageNumber)) {
			pages = pageNumber;
		}

		let size = 10;

		if (limitNumber > ZERO && !Number.isNaN(limitNumber)) {
			size = limitNumber;
		}
		try {
			const count = await product.count();

			const products = await product
				.find({})
				.limit(size)
				.skip(size * pages);

			res.status(SUCCESS).send({
				data: products,
				totalPages: Math.ceil(count / size),
			});
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
	getProductByCategory: async (req, res) => {
		const { category } = req.params;
		const { limit, page } = req.query;
		const pageNumber = Number.parseInt(page, 10);
		const limitNumber = Number.parseInt(limit, 10);
		let pages = 0;
		let size = 10;

		if (pageNumber > ZERO && !Number.isNaN(pageNumber)) {
			pages = pageNumber;
		}
		if (limitNumber > ZERO && !Number.isNaN(limitNumber)) {
			size = limitNumber;
		}
		if (!category) {
			res.status(BAD_REQUEST).send({ error: "Bad Request" });
		}
		try {
			const count = await product
				.find({
					category: category,
				})
				.count();

			const categoryProducts = await product
				.find({
					category: category,
				})
				.limit(size)
				.skip(size * pages);
			if (categoryProducts[ZERO]) {
				res.status(SUCCESS).send({
					data: categoryProducts,
					totalPages: Math.ceil(count / size),
				});
			} else {
				res.status(NOT_FOUND).send({ error: "Inexistant Category" });
			}
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
	getProductById: async (req, res) => {
		const { id } = req.params;

		if (!id) {
			res.status(BAD_REQUEST).send({ error: "Bad Request" });
		}

		try {
			const oneProduct = await product.findById(id);
			res.status(SUCCESS).send({ data: [ oneProduct ] });
		} catch (e) {
			log.error(e);
			res.status(NOT_FOUND).send({ error: "Product not found" });
		}
	},
	postProduct: async (req, res) => {
		const productBody = req.body;
		if (!productBody.name) {
			res.status(BAD_REQUEST).send({ error: "Bad Request" });
		} else {
			try {
				const newProduct = await product.create({
					name: productBody.name,
					price: productBody.price,
					description: productBody.description,
					category: productBody.category,
				});
				res.status(CREATED).send({ data: newProduct });
			} catch (e) {
				log.error(e);
				res.status(INTERNAL_SERVER_ERROR).send({ error: e });
			}
		}
	},
};

export default productController;
