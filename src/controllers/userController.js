import mongoose from "mongoose";
import log from "loglevel";

import user from "../models/user.js";
import CODES from "../constantes/httpCodes.js";

const ZERO = 0;
const { SUCCESS, INTERNAL_SERVER_ERROR, NOT_FOUND, CREATED, BAD_REQUEST } =
  CODES;

const userController = {
	getAll: async (req, res) => {
		try {
			const users = await user.find({});

			if (users !== []) {
				res.status(SUCCESS).send({ data: users, totalUsers: users.length });
			}
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
	getById: async (req, res) => {
		const { id } = req.params;
		try {
			if (mongoose.Types.ObjectId.isValid(id)) {
				const oneUser = await user.findById(id);
				res.status(SUCCESS).send({ data: oneUser });
			} else {
				res.status(NOT_FOUND).send({ error: "User Not Found" });
			}
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
	getCartById: async (req, res) => {
		const { id } = req.params;
		try {
			if (mongoose.Types.ObjectId.isValid(id)) {
				const oneUser = await user.findById(id);
				res.status(SUCCESS).send({ data: oneUser.cart });
			} else {
				res.status(NOT_FOUND).send({ error: "User Not Found" });
			}
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
	getFavsById: async (req, res) => {
		const { id } = req.params;
		try {
			if (mongoose.Types.ObjectId.isValid(id)) {
				const oneUser = await user.findById(id);
				res.status(SUCCESS).send({ data: oneUser.favs });
			} else {
				res.status(NOT_FOUND).send({ error: "User Not Found" });
			}
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
	getOrdersById: async (req, res) => {
		const { id } = req.params;
		try {
			if (mongoose.Types.ObjectId.isValid(id)) {
				const oneUser = await user.findById(id);
				res.status(SUCCESS).send({ data: oneUser.orders });
			} else {
				res.status(NOT_FOUND).send({ error: "User Not Found" });
			}
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
	postUser: async (req, res) => {
		const one = 1;
		let userBody;

		if (res.locals.currUser && !res.locals.currUser.error) {
			userBody = res.locals.currUser;
		} else {
			userBody = Object.keys(req.body).length > one ? req.body : null;
		}

		try {
			if (userBody) {
				const newUser = await user.create({
					pseudo: userBody.pseudo,
					firstName: userBody.firstName,
					lastName: userBody.lastName,
					email: userBody.email,
					socialTitle: userBody.socialTitle,
					phone: userBody.phone,
					addresses: userBody.addresses,
					password: userBody.password,
				});
				res.status(CREATED).send({ data: newUser });
			} else {
				res.status(BAD_REQUEST).send({ error: "Bad Request" });
			}
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
	putUserById: async (req, res) => {
		const userBody = Object.keys(req.body).length > ZERO ? req.body : null;
		const { id } = req.params;
		try {
			if (mongoose.Types.ObjectId.isValid(id) && userBody) {
				const newUser = await user.findOneAndUpdate(
					{ _id: id },
					{
						...userBody,
					},
					{ new: true }
				);
				res.status(CREATED).send({ data: newUser });
			} else {
				res.status(BAD_REQUEST).send({ error: "Bad Request" });
			}
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
	deleteUserById: async (req, res) => {
		const { id } = req.params;
		try {
			if (mongoose.Types.ObjectId.isValid(id)) {
				const deletedUser = await user.deleteOne({ _id: id });
				res.status(SUCCESS).send({ data: deletedUser });
			} else {
				res.status(BAD_REQUEST).send({ error: "Bad Request" });
			}
		} catch (e) {
			log.error(e);
			res.status(INTERNAL_SERVER_ERROR).send({ error: e });
		}
	},
};

export default userController;
