import argon2 from "argon2";
import jwt from "jsonwebtoken";
import log from "loglevel";
import dotenv from "dotenv";
dotenv.config();

import user from "../models/user.js";
import CODES from "../constantes/httpCodes.js";

const { SUCCESS, UNAUTHORIZED, BAD_REQUEST } = CODES;
const TWO = 2;
const SIXTEEN = 16;

const hashingOptions = {
	type: argon2.argon2id,
	memoryCost: TWO ** SIXTEEN,
	timeCost: 5,
	parallelism: 1,
};

const authController = {
	hashPassword: async (req, res, next) => {
		const newUser = res.locals.currUser;
		try {
			const hashedPassword = await argon2.hash(
				newUser.password,
				hashingOptions
			);
			newUser.password = hashedPassword;
			res.locals.currUser = newUser;
			next();
		} catch (e) {
			res.locals.currUser = { error: "error hashing password" };
			next();
		}
	},
	verifyPassword: async (req, res) => {
		const notHashedPassword = req.body.password;
		const hashedPassword = res.locals.userTry.password;
		const isVerified = await argon2.verify(
			hashedPassword,
			notHashedPassword,
			hashingOptions
		);
		if (isVerified) {
			const token = jwt.sign(
				{ sub: res.locals.userTry._id },
				process.env.JWT_SECRET,
				{
					algorithm: "HS512",
				}
			);
			const userToSend = res.locals.userTry;
			userToSend.password = "Salut l'ami";
			res.status(SUCCESS).send({ token, data: userToSend });
		} else {
			res.status(UNAUTHORIZED).send({ error: "Unauthorized" });
		}
	},
	verifyToken: (req, res, next) => {
		try {
			const [ type, token ] = req.headers.authorization.split(" ");
			if (type !== "Bearer") {
				throw new Error("Only Bearer token allowed");
			}
			req.payload = jwt.verify(token, process.env.JWT_SECRET);
			next();
		} catch (err) {
			log.error(err);
			res.status(UNAUTHORIZED).send({ error: "Unauthorized" });
		}
	},
	getUserByEmailForRegister: async (req, res, next) => {
		const { email } = req.body;
		const isUserFind = Boolean(await user.findOne({ email: email }));
		if (isUserFind) {
			res.status(BAD_REQUEST).send({ error: "User already registered" });
		} else {
			res.locals.currUser = req.body;
			next();
		}
	},
	getUserByEmailForLogin: async (req, res, next) => {
		const { email } = req.body;
		const isUserFind = Boolean(await user.findOne({ email: email }));
		if (!isUserFind || !req.body.password) {
			res.status(BAD_REQUEST).send({ error: "Bad email or empty password" });
		} else {
			res.locals.userTry = await user.findOne({ email: email });
			next();
		}
	},
};

export default authController;
