import router from "../router.js";
import authController from "../controllers/authController.js";
import userController from "../controllers/userController.js";

const authRouter = router;

authRouter.post(
	"/api/auth/register",
	authController.getUserByEmailForRegister,
	authController.hashPassword,
	userController.postUser
);
authRouter.post(
	"/api/auth/login",
	authController.getUserByEmailForLogin,
	authController.verifyPassword
);

export default authRouter;
