import router from "../router.js";
import userController from "../controllers/userController.js";

const userRouter = router;

userRouter.get("/api/users", userController.getAll);
userRouter.get("/api/users/:id", userController.getById);
userRouter.get("/api/users/:id/cart", userController.getCartById);
userRouter.get("/api/users/:id/favs", userController.getFavsById);
userRouter.get("/api/users/:id/orders", userController.getOrdersById);
userRouter.post("/api/users", userController.postUser);
userRouter.put("/api/users/:id", userController.putUserById);
userRouter.delete("/api/users/:id", userController.deleteUserById);

export default userRouter;
