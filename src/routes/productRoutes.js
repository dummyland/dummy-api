import router from "../router.js";
import productController from "../controllers/productController.js";

const productRouter = router;

productRouter.get("/api/products/", productController.getAllProduct);
productRouter.get("/api/products/id/:id", productController.getProductById);
productRouter.get(
	"/api/products/category/:category",
	productController.getProductByCategory
);
productRouter.post("/api/products", productController.postProduct);

export default productRouter;
