import CODES from "../constantes/httpCodes.js";

const { BAD_REQUEST } = CODES;

function badRoutes(req, res, next) {
	const host = req.path + Object.values(req.params);
	if (
		host.endsWith("/id") ||
    host.endsWith("/id/") ||
    host.endsWith("/category") ||
    host.endsWith("category/")
	) {
		res.status(BAD_REQUEST).send({ error: "Bad Request" });
	} else {
		next();
	}
}

export default badRoutes;
