import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();

function getUri() {
  if (process.env.ENV === "PROD") {
    return process.env.PROD_URI;
  } else {
    return process.env.ENV === "TEST" ? process.env.TEST_URI : process.env.URI;
  }
}

const URI = getUri();

async function main() {
  await mongoose.connect(URI);
}

export default main;
