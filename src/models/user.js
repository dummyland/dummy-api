import { Schema, model } from "mongoose";
import pkg from "validator";
const { isEmail, isMobilePhone } = pkg;

import { productSchema } from "./product.js";
import { orderSchema } from "./order.js";
import addressSchema from "./address.js";
import { cartSchema } from "./cart.js";

const userSchema = Schema(
	{
		pseudo: { type: String, required: true },
		firstName: String,
		lastName: String,
		email: {
			type: String,
			required: true,
			unique: true,
			validate: [ isEmail, "invalid email" ],
		},
		password: { type: String, required: true },
		socialTitle: String,
		phone: { type: String, validate: [ isMobilePhone, "invalide phone number" ] },
		addresses: [ addressSchema ],
		cart: cartSchema,
		favs: [ productSchema ],
		orders: [ orderSchema ],
	},
	{ timestamps: true }
);

const user = model("user", userSchema);

export default user;
