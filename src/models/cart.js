import { model, Schema } from "mongoose";
import { productSchema } from "./product.js";

export const cartSchema = Schema({
	items: [
		{
			quantity: { type: Number, required: true },
			item: { type: productSchema, required: true },
		},
	],
	totalItems: Number,
	total: Number,
});

const cart = model("cart", cartSchema);

export default cart;
