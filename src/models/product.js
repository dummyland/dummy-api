import { Schema, model } from "mongoose";

export const productSchema = Schema(
	{
		name: { type: String, required: true },
		price: {
			original: { type: Number, required: true },
			actual: Number,
			reduction: String,
		},
		images: [],
		category: String,
		description: String,
		features: {},
	},
	{ timestamps: true }
);

const product = model("product", productSchema);

export default product;
