import { Schema } from "mongoose";

const addressSchema = Schema({
	name: { type: String, required: true },
	numero: Number,
	streetName: { type: String, required: true },
	complementaryAddressInfo: String,
	postalCode: { type: String, required: true },
	town: { type: String, required: true },
	country: { type: String, required: true },
});

export default addressSchema;
