import { Schema, model } from "mongoose";
import pkg from "validator";
const { isEmail, isMobilePhone } = pkg;

import addressSchema from "./address.js";
import { cartSchema } from "./cart.js";

export const orderSchema = Schema({
	cart: cartSchema,

	socialTitle: { type: String, required: true },
	firstName: { type: String, required: true },
	lastName: { type: String, required: true },
	email: { type: String, required: true, validate: [ isEmail, "invalid email" ] },
	phone: { type: String, validate: [ isMobilePhone, "invalide phone number" ] },

	shipping: addressSchema,
	shippingMethod: { type: String, required: true },

	paymentMethod: { type: String, required: true },
	paymentDate: { type: Date, required: true, default: Date.now },
	paymentCard: {
		holderName: { type: String, required: true },
		cvv: { type: Number, required: true },
		expirationDate: { type: Date, required: true },
		cardNumber: { type: Number, required: true },
	},
});

const order = model("order", orderSchema);

export default order;
