import chai from "chai";
const { expect: chaiExcept } = chai;

import main from "../src/db.js";
import user from "../src/models/user.js";
import product from "../src/models/product.js";
import { testUsers } from "../common/testUsers.js";
import { testProduct } from "../common/testProduct.js";

let users;
let products;

describe("ROUTES", () => {
  beforeEach(async () => {
    await main();
  });
  describe("Product", () => {
    beforeEach(async () => {
      await product.deleteMany({});
      products = await product.insertMany(testProduct);
    });
    it("GET /api/products should return 200", async () => {
      const res = await fetch("http://127.0.0.1:4000/api/products");
      chaiExcept(res.status).to.equal(200);
    });
    it("GET /api/products/id/:id should return 200", async () => {
      const randomProduct = products[3];
      const res = await fetch(
        `http://127.0.0.1:4000/api/products/id/${randomProduct._id}`
      );
      chaiExcept(res.status).to.equal(200);
    });
    it("GET /api/products/id/:badId should return 404", async () => {
      const res = await fetch("http://127.0.0.1:4000/api/products/id/abcde");
      chaiExcept(res.status).to.equal(404);
    });
    it("GET /api/products//id/:emptyId should return 400 ", async () => {
      const res = await fetch("http://127.0.0.1:4000/api/products/id/");
      chaiExcept(res.status).to.equal(400);
    });
    it("GET /api/products/category/:category should return 200", async () => {
      const res = await fetch(
        "http://127.0.0.1:4000/api/products/category/fork"
      );
      chaiExcept(res.status).to.equal(200);
    });
    it("GET /api/products/category/:badCategory should return 404 ", async () => {
      const res = await fetch(
        "http://127.0.0.1:4000/api/products/category/roller"
      );
      chaiExcept(res.status).to.equal(404);
    });
    it("GET /api/products/category/:emptyCategory should return 400", async () => {
      const res = await fetch("http://127.0.0.1:4000/api/products/category");
      chaiExcept(res.status).to.equal(400);
    });
    it("POST /api/products/ should return 201", async () => {
      const newProduct = {
        name: "Patch Dissidence",
        price: {
          original: "7.90",
        },
        images: [
          {
            color: "dissidence",
            pic: [
              "https://dissidencescootershop.com/img/m/62.jpg",
              "https://dissidencescootershop.com/8388-home_default/patch-dissidence.jpg",
            ],
          },
        ],
        description:
          "- Iron-on: Can stick to all fabrics.- Made in France- Diameter: 70mm",
        features: {},
        category: "accessories",
      };
      const fetchOpts = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newProduct),
      };
      const res = await fetch("http://127.0.0.1:4000/api/products/", fetchOpts);
      chaiExcept(res.status).to.equal(201);
    });
    //TODO: More Error Test + DBError Test
    it("POST /api/products should return 400 if there is no body", async () => {
      const fetchOpts = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      };
      const res = await fetch("http://127.0.0.1:4000/api/products/", fetchOpts);
      chaiExcept(res.status).to.equal(400);
    });
  });
  describe("User", () => {
    beforeEach(async function () {
      await user.deleteMany({});
      users = await user.insertMany(testUsers);
    });
    it("GET /api/users should return 200", async () => {
      const res = await fetch("http://127.0.0.1:4000/api/users");
      chaiExcept(res.status).to.equal(200);
    });
    it("GET /api/users/:id should return 200", async () => {
      //TODO: When auth will be done, test if route are protected
      const randomUser = users[2];
      const res = await fetch(
        `http://127.0.0.1:4000/api/users/${randomUser._id}`
      );
      chaiExcept(res.status).to.equal(200);
    });
    it("GET /api/users/id/:badId should return 404", async () => {
      const res = await fetch(`http://127.0.0.1:4000/api/users/lucyrasil`);
      chaiExcept(res.status).to.equal(404);
    });
    it("GET /api/users/:id/cart should return 200", async () => {
      //TODO: When auth will be done, test if route are protected
      const randomUser = users[1];
      const res = await fetch(
        `http://127.0.0.1:4000/api/users/${randomUser._id}/cart`
      );
      chaiExcept(res.status).to.equal(200);
    });
    it("GET /api/users/:id/favs should return 200", async () => {
      //TODO: When auth will be done, test if route are protected
      const randomUser = users[0];
      const res = await fetch(
        `http://127.0.0.1:4000/api/users/${randomUser._id}/favs`
      );
      chaiExcept(res.status).to.equal(200);
    });
    it("GET /api/users/:id/orders should return 200", async () => {
      //TODO: When auth will be done, test if route are protected
      const randomUser = users[2];
      const res = await fetch(
        `http://127.0.0.1:4000/api/users/${randomUser._id}/orders`
      );
      chaiExcept(res.status).to.equal(200);
    });
    it("POST /api/users should return 201", async () => {
      const newUser = {
        pseudo: "Cyrheel",
        firstName: "cyril",
        lastName: "b",
        email: "x@x.com",
        password: "etyo",
        socialTitle: "Mr",
        phone: 781819930,
        addresses: [],
        cart: [],
        favs: [],
        orders: [],
      };
      const fetchOpts = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newUser),
      };
      const res = await fetch("http://127.0.0.1:4000/api/users", fetchOpts);
      chaiExcept(res.status).to.equal(201);
    });
    it("PUT /api/users/:id should return 201", async () => {
      const randomUser = users[1];
      const toModify = {
        pseudo: "George",
      };
      const fetchOpts = {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(toModify),
      };
      const res = await fetch(
        `http://127.0.0.1:4000/api/users/${randomUser._id}`,
        fetchOpts
      );
      chaiExcept(res.status).to.equal(201);
    });
    it("DELETE /api/users/:id should return 200", async () => {
      const randomUser = users[0];
      const fetchOpts = {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      };
      const res = await fetch(
        `http://127.0.0.1:4000/api/users/${randomUser._id}`,
        fetchOpts
      );
      chaiExcept(res.status).to.equal(200);
    });
    // TODO: ADD Error handling tests !
  });
  describe("Auth", () => {
    beforeEach(async function () {
      await user.deleteMany({});
    });
    it("POST /api/auth/register should return 201 when the new user is created", async () => {
      const newUser = {
        pseudo: "Test",
        firstName: "John",
        lastName: "Doe",
        email: "testroutes@email.com",
        password: "testroutes",
        socialTitle: "Mr",
        phone: "0606060606",
        adresses: {
          name: "Me",
          numero: 7,
          streetName: "Test Street",
          postalCode: "777777",
          town: "DummyVille",
          country: "DummyLand",
        },
      };
      const body = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(newUser),
      };
      const res = await fetch("http://127.0.0.1:4000/api/auth/register", body);
      chaiExcept(res.status).to.equal(201);
    });
    it("POST /auth/register should return status 400 when user already exist", async () => {
      const newUser = {
        pseudo: "Test",
        firstName: "John",
        lastName: "Doe",
        email: "testroutes@email.com",
        password: "testroutes",
        socialTitle: "Mr",
        phone: "0606060606",
        adresses: {
          name: "Me",
          numero: 7,
          streetName: "Test Street",
          postalCode: "777777",
          town: "DummyVille",
          country: "DummyLand",
        },
      };
      const createFetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(newUser),
      };
      const createRes = await fetch(
        "http://127.0.0.1:4000/api/auth/register",
        createFetchOpts
      );
      const randomUser = await createRes.json();
      const payload = { email: randomUser.data.email };
      const fetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(payload),
      };

      const res = await fetch(
        "http://127.0.0.1:4000/api/auth/register",
        fetchOpts
      );
      chaiExcept(res.status).to.equal(400);
    });
    it("POST /api/auth/login should return 200 when the new user is logged", async () => {
      const newUser = {
        pseudo: "Test",
        firstName: "John",
        lastName: "Doe",
        email: "testroutes@email.com",
        password: "testroutes",
        socialTitle: "Mr",
        phone: "0606060606",
        adresses: {
          name: "Me",
          numero: 7,
          streetName: "Test Street",
          postalCode: "777777",
          town: "DummyVille",
          country: "DummyLand",
        },
      };
      const createFetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(newUser),
      };
      const createRes = await fetch(
        "http://127.0.0.1:4000/api/auth/register",
        createFetchOpts
      );
      const randomUser = await createRes.json();
      const payload = {
        email: "testroutes@email.com",
        password: "testroutes",
      };
      const fetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(payload),
      };
      const res = await fetch(
        "http://127.0.0.1:4000/api/auth/login",
        fetchOpts
      );
      chaiExcept(res.status).to.equal(200);
    });
    it("POST /api/auth/login return 400 when email is not found", async () => {
      const body = {
        email: "inconu@unknown.com",
      };
      const fetchOpts = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      };
      const res = await fetch(
        "http://127.0.0.1:4000/api/auth/login",
        fetchOpts
      );
      chaiExcept(res.status).to.equal(400);
    });
  });
});
