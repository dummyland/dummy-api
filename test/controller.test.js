import chai from "chai";
const { expect: chaiExcept } = chai;

import main from "../src/db.js";
import user from "../src/models/user.js";
import product from "../src/models/product.js";
import { testUsers } from "../common/testUsers.js";
import { testProduct } from "../common/testProduct.js";

let users;
let products;

describe("CONTROLLERS", () => {
  beforeEach(async () => {
    await main();
  });
  describe("Product", () => {
    beforeEach(async () => {
      await product.deleteMany({});
      products = await product.insertMany(testProduct);
    });
    it("GET /api/products should return 5 products & 1 pages by default", async () => {
      let res = await fetch("http://127.0.0.1:4000/api/products");
      res = await res.json();
      chaiExcept(res.data.length).to.equal(5);
      chaiExcept(res.totalPages).to.equal(1);
    });
    it("GET /api/products/id/:id should return 1 product", async () => {
      const randomProduct = products[3];
      let res = await fetch(
        `http://127.0.0.1:4000/api/products/id/${randomProduct._id}`
      );
      res = await res.json();
      chaiExcept(res.data.length).to.equal(1);
    });
    it("GET /api/products/id/:id should return correct error message when bad id or empty id is passed", async () => {
      let res = await fetch("http://127.0.0.1:4000/api/products/id");
      res = await res.json();
      chaiExcept(res.error).to.equal("Bad Request");
    });
    it("GET /api/products/category/:category should return 10 products & first element is a fork", async () => {
      let res = await fetch("http://127.0.0.1:4000/api/products/category/fork");
      res = await res.json();
      chaiExcept(res.data.length).to.equal(1);
      chaiExcept(res.data[0].category).to.equal("fork");
    });
    it("GET /api/products/category/:category should return correct error message when bad input is provided", async () => {
      const res = await fetch(
        "http://127.0.0.1:4000/api/products/category/roller"
      );
      const data = await res.json();
      chaiExcept(data.error).to.equal("Inexistant Category");
    });
    it("GET /api/products/category should return correct error message when empty input is provided", async () => {
      const res = await fetch("http://127.0.0.1:4000/api/products/category/");
      const data = await res.json();
      chaiExcept(data.error).to.equal("Bad Request");
    });
  });
  describe("User", () => {
    beforeEach(async () => {
      await user.deleteMany({});
      users = await user.insertMany(testUsers);
    });
    it("GET /api/users should return 3 users & 1 pages by default", async () => {
      let res = await fetch("http://127.0.0.1:4000/api/users");
      res = await res.json();
      chaiExcept(res.data.length).to.equal(3);
    });
    it("GET /api/users/:id should return 1 users", async () => {
      let res = await fetch(`http://127.0.0.1:4000/api/users/${users[1]._id}`);
      res = await res.json();
      chaiExcept(res.data.name).to.equal(users[1].name);
    });
    it("GET /api/users/:BadId should return correct error message", async () => {
      let res = await fetch(`http://127.0.0.1:4000/api/users/092`);
      res = await res.json();
      chaiExcept(res.error).to.equal("User Not Found");
    });
    it("GET /api/users/:id/cart should return 1 cart", async () => {
      let res = await fetch(
        `http://127.0.0.1:4000/api/users/${users[1]._id}/cart`
      );
      res = await res.json();
      chaiExcept(Object.keys(res.data)).to.deep.equal([
        "items",
        "totalItems",
        "total",
        "_id",
      ]);
    });
    it("GET /api/users/:BadId/cart should return correct error message", async () => {
      let res = await fetch(`http://127.0.0.1:4000/api/users/092/cart`);
      res = await res.json();
      chaiExcept(res.error).to.equal("User Not Found");
    });
    it("GET /api/users/:id/favs should return 1 favs", async () => {
      let res = await fetch(
        `http://127.0.0.1:4000/api/users/${users[0]._id}/favs`
      );
      res = await res.json();
      chaiExcept(res.data).to.deep.equal(users[0].favs);
    });
    it("GET /api/users/:BadId/cart should return correct error message", async () => {
      let res = await fetch(`http://127.0.0.1:4000/api/users/092/cart`);
      res = await res.json();
      chaiExcept(res.error).to.equal("User Not Found");
    });
    it("GET /api/users/:id/orders should return 1 orders", async () => {
      let res = await fetch(
        `http://127.0.0.1:4000/api/users/${users[2]._id}/orders`
      );
      res = await res.json();
      chaiExcept(res.data).to.deep.equal(users[2].orders);
    });
    it("GET /api/users/:BadId/orders should return correct error message", async () => {
      let res = await fetch(`http://127.0.0.1:4000/api/users/092/orders`);
      res = await res.json();
      chaiExcept(res.error).to.equal("User Not Found");
    });
    it("POST /api/users should return 1 created user", async () => {
      const payload = {
        pseudo: "Test",
        firstName: "John",
        lastName: "Doe",
        email: "testroutes@email.com",
        password: "testroutes",
        socialTitle: "Mr",
        phone: "0606060606",
        adresses: {
          name: "Me",
          numero: 7,
          streetName: "Test Street",
          postalCode: "777777",
          town: "DummyVille",
          country: "DummyLand",
        },
      };
      const fetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(payload),
      };
      let res = await fetch(`http://127.0.0.1:4000/api/users`, fetchOpts);
      res = await res.json();
      chaiExcept(res.data.pseudo).to.equal(payload.pseudo);
    });
    it("POST /api/users should return correct error code", async () => {
      const fetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
      };
      let res = await fetch(`http://127.0.0.1:4000/api/users`, fetchOpts);
      res = await res.json();
      chaiExcept(res.error).to.equal("Bad Request");
    });
    it("PUT /api/users/:id should return updated user", async () => {
      const payload = { pseudo: "YOOLOOO" };
      const fetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "PUT",
        body: JSON.stringify(payload),
      };
      let res = await fetch(
        `http://127.0.0.1:4000/api/users/${users[0]._id}`,
        fetchOpts
      );
      res = await res.json();
      chaiExcept(res.data.pseudo).to.equal(payload.pseudo);
    });
    it("PUT /api/users/:id should return correct error message", async () => {
      const fetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "PUT",
      };
      let res = await fetch(
        `http://127.0.0.1:4000/api/users/${users[0]._id}`,
        fetchOpts
      );
      res = await res.json();
      chaiExcept(res.error).to.equal("Bad Request");
    });
    it("DELETE /api/users/:id should return deleted user", async () => {
      const fetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "DELETE",
      };
      let res = await fetch(
        `http://127.0.0.1:4000/api/users/${users[0]._id}`,
        fetchOpts
      );
      res = await res.json();
      chaiExcept(res.data.deletedCount).to.equal(1);
    });
    it("DELETE /api/users/:id should return correct error message", async () => {
      const fetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "DELETE",
      };
      let res = await fetch(
        `http://127.0.0.1:4000/api/users/nobody`,
        fetchOpts
      );
      res = await res.json();
      chaiExcept(res.error).to.equal("Bad Request");
    });
  });
  describe("Auth", () => {
    beforeEach(async () => {
      await user.deleteMany({});
    });
    it("POST /api/auth/register should return the new user created", async () => {
      const newUser = {
        pseudo: "qzkejfb",
        firstName: "John",
        lastName: "Doe",
        email: "johndoe@email.com",
        password: "password",
        socialTitle: "Mr",
        phone: "0606060606",
        adresses: {
          name: "Me",
          numero: 7,
          streetName: "Test Street",
          postalCode: "777777",
          town: "DummyVille",
          country: "DummyLand",
        },
      };

      const body = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(newUser),
      };
      let res = await fetch("http://127.0.0.1:4000/api/auth/register", body);
      res = await res.json();
      chaiExcept(res.data.pseudo).to.equal("qzkejfb");
    });
    it("POST /api/auth/login should return a token", async () => {
      const payload = {
        pseudo: "qzkejfb",
        firstName: "John",
        lastName: "Doe",
        email: "e@e.com",
        password: "etyo",
        socialTitle: "Mr",
        phone: "0606060606",
        adresses: {
          name: "Me",
          numero: 7,
          streetName: "Test Street",
          postalCode: "777777",
          town: "DummyVille",
          country: "DummyLand",
        },
      };

      const createFetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(payload),
      };
      let createRes = await fetch(
        "http://127.0.0.1:4000/api/auth/register",
        createFetchOpts
      );
      createRes = await createRes.json();
      const body = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({
          email: "e@e.com",
          password: "etyo",
        }),
      };

      let res = await fetch("http://127.0.0.1:4000/api/auth/login", body);
      res = await res.json();

      chaiExcept(res.token).to.have.lengthOf.above(20);
    });
    it("POST /auth/login should have no token and return an not found error", async () => {
      const newUser = {
        email: "notregistereduser@email.com",
        password: "notregistered",
      };

      const body = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(newUser),
      };

      let res = await fetch("http://127.0.0.1:4000/api/auth/login", body);
      res = await res.json();

      chaiExcept(res.token).to.be.undefined;
      chaiExcept(res.error).to.equal("Bad email or empty password");
    });
    it("POST /auth/login should not return a token if wrong password", async () => {
      const payload = {
        pseudo: "qzkejfb",
        firstName: "John",
        lastName: "Doe",
        email: "e@e.com",
        password: "etyo",
        socialTitle: "Mr",
        phone: "0606060606",
        adresses: {
          name: "Me",
          numero: 7,
          streetName: "Test Street",
          postalCode: "777777",
          town: "DummyVille",
          country: "DummyLand",
        },
      };

      const createFetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(payload),
      };
      let createRes = await fetch(
        "http://127.0.0.1:4000/api/auth/register",
        createFetchOpts
      );
      createRes = await createRes.json();

      const tryPayload = {
        email: "e@e.com",
        password: "wrongpassword",
      };

      const fetchOpts = {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(tryPayload),
      };

      let res = await fetch("http://127.0.0.1:4000/api/auth/login", fetchOpts);
      res = await res.json();

      chaiExcept(res.token).to.be.undefined;
      chaiExcept(res.error).to.equal("Unauthorized");
    });
  });
});
