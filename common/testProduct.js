export const testProduct = [
  {
    name: "TV Velocity II Headset",
    description:
      "Our Velocity II Headset harnesses everything you guys loved from our original one. While rocking a more modern and up-to-date look. We decided to swap out the original title of Velocity on the dust cover and replaced it with 2 more Iconic TV circle logos to ensure visibility from any angle. For $20 not only with you have one of the smoothest spinning scooters out there, but you'll also be repping your favorite shop!",
    price: {
      original: 18.75,
    },
    features: {},
    images: [
      {
        color: "Black",
        pic: [
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_black_01.jpg",
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_black_01_1_2.jpg",
        ],
      },
      {
        color: "Blue",
        pic: [
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_blue_01.jpg",
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_blue_01_1.jpg",
        ],
      },
      {
        color: "Neo Chrome",
        pic: [
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_neo_chrome_01.jpg",
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_neo_chrome_01_1.jpg",
        ],
      },
      {
        color: "Red",
        pic: [
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_red_01_1.jpg",
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_red_01_1_1.jpg",
        ],
      },
      {
        color: "Silver",
        pic: [
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_silver_01.jpg",
          "https://www.thevaultproscooters.com/media/catalog/product/cache/c2bc086a59250190712e74b763238f11/t/v/tv_velocity_ii_-_silver_01_1.jpg",
        ],
      },
    ],
    category: "headset",
  },
  {
    name: "Envy Delux Wheel",
    description:
      "The Envy Delux wheel is sold individually, comes in at 120mm in diameter, 24mm wide at the core, but 26mm wide at the urethane. The increased width is designed to make your grinds more consistent as it puts extra distance between your wheel's core, and the obstacle that you're grinding. This wheel will fit any fork that is capable of fitting 120mm wheels and is a must have if you're looking to add some color to your setup.",
    price: {
      original: 46.95,
    },
    features: {
      weight: 271.8,
    },
    images: [],
    category: "wheel",
  },
  {
    name: "Fork Striker Revus SCS HIC Neochrome",
    price: {
      original: "74.90",
      actual: "59.90",
      reduction: "-15.00%",
    },
    images: [
      {
        color: "dissidence",
        pic: [
          "https://dissidencescootershop.com/6887-home_default/fork-striker-revus-scs-hic-neochrome.jpg",
          "https://dissidencescootershop.com/5135-home_default/fork-striker-revus-scs-hic-neochrome.jpg",
          "https://dissidencescootershop.com/6888-home_default/fork-striker-revus-scs-hic-neochrome.jpg",
          "https://dissidencescootershop.com/img/m/109.jpg",
        ],
      },
    ],
    description: null,
    features: {
      material: ["Alu 7075"],
      weight: "303",
      std12Compat: false,
      compressionType: ["HIC", "SCS"],
      wheelDiameter: ["125"],
      wheelWidth: ["24"],
    },
    category: "fork",
  },
  {
    name: "Griptape Triad Logo repeat",
    price: {
      actual: "10.50",
      original: "15.00",
      reduction: "-30%",
    },
    images: [
      {
        color: "dissidence",
        pic: [
          "https://dissidencescootershop.com/img/m/139.jpg",
          "https://dissidencescootershop.com/5624-home_default/griptape-triad-logo-repeat.jpg",
        ],
      },
    ],
    description:
      "Grip très original avec un mix de grip transparent. Effet garanti sur ton deck.",
    features: {
      griptapeLength: "575.00",
      griptapeWidth: "147.50",
      grainType: "Regular",
    },
    category: "griptape",
  },
  {
    name: "Scooter Versatyl Bloody Mary V2 Black",
    price: {
      original: "149.00",
    },
    images: [
      {
        color: "dissidence",
        pic: [
          "https://dissidencescootershop.com/5904-home_default/scooter-versatyl-bloody-mary-v2-black.jpg",
          "https://dissidencescootershop.com/7763-home_default/scooter-versatyl-bloody-mary-v2-black.jpg",
          "https://dissidencescootershop.com/7762-home_default/scooter-versatyl-bloody-mary-v2-black.jpg",
          "https://dissidencescootershop.com/7761-home_default/scooter-versatyl-bloody-mary-v2-black.jpg",
          "https://dissidencescootershop.com/img/m/143.jpg",
        ],
      },
    ],
    description:
      "Versatyl is a brand designed by the creator of Ethic.\nThey are entry-level trotters but of quality with an unbeatable price / quality ratio. Always looking for weight optimization for more responsiveness.\nOn this V2 version the compression system is now in IHC. The deck is wider. It doesn't show on the design which is the same, but there have been lots of small modifications to gain strength and weight.\nThe bloody mary has aluminum handlebars and dimensions larger than the cosmopolitan. Perfect for beginners.",
    features: {
      angle: "83",
      material: ["Alu 6061"],
      diameter: ["34.9"],
      backsweep: false,
      hardness: "88A",
      weight: "3150",
      deckLength: ["502.50"],
      deckWidth: ["116.25"],
      std12Compat: false,
      deckEndType: "Regular",
      barHeight: ["590.00"],
      barWidth: ["532.50"],
      compressionType: ["IHC"],
      wheelDiameter: ["110"],
      wheelWidth: ["24"],
      grainType: "Regular",
    },
    category: "fullbuild",
  },
];
